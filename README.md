# sairem conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/sairem"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS sairem module
